// This is the custom JavaScript file referenced by index.html. You will notice
// that this file is currently empty. By adding code to this empty file and
// then viewing index.html in a browser, you can experiment with the example
// page or follow along with the examples in the book.
//
// See README.txt for more information.
$(document).ready(function() {
	$("div.chapter p").each(function(index) {
		if (index > 2) {
			$('<a href="#top">back to top<a/>').insertAfter($(this));
		}
	});
	$("<a id=top></a>").prependTo("body");
});

$(document).ready(function() {
	$("div.chapter a").on("click", function() {
		$('<p>You were here</p>').insertAfter($(this));
	});
});

$(document).ready(function() {
	$('div[id*="author"]').on("click", function() {
		if($(this).parent().prop('tagName') == 'B') {
			$(this).unwrap();
		}
		else {
			$(this).wrap('<b></b>');
		}
	});
});

$(document).ready(function() {
	$('.chapter p').each(function() {
		var currentClass = $(this).prop('class');
		var newClass = currentClass += ' inhabitants';
		$(this).attr({
			class: newClass
		});
	});
}); 