from django.db import models

# Create your models here.

class FruitSalad(models.Model):
	components = models.CharField(max_length=40)
	def __str__(self):
		return self.components
