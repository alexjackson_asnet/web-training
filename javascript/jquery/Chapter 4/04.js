// This is the custom JavaScript file referenced by index.html. You will notice
// that this file is currently empty. By adding code to this empty file and
// then viewing index.html in a browser, you can experiment with the example
// page or follow along with the examples in the book.
//
// See README.txt for more information.

/* Practice during the reading
$(document).ready(function() {
	$("#switcher button").on("click", function() {
		$speech = $("div.speech");
		var fs = parseFloat($speech.css("fontSize"));
		if (this.id == "switcher-large") {
			fs*=1.4;
		} else if (this.id =="switcher-small") {
			fs/=1.4;
		} else {
			fs = 14;
		}
		$speech.css("fontSize", fs + "px");
	});
});

$(document).ready(function() {
	$("p").eq(1).hide();
	$("a.more").on("click", function(event) {
		event.preventDefault();
		$("p").eq(1).slideDown("slow");
		$(this).hide();
	}); 
});

$(document).ready(function() {
	var $switcher = $("div#switcher");
	$switcher.on("click", function() {
		$switcher.css({
			borderColor: "blue"
		}).animate({
				borderWidth: "5px",
			}, "slow");
	});
});
*/

$(document).ready(function(){
	$("body").hide();
	$("body").fadeIn("slow");
})

$(document).ready(function() {
	$("p").hover(
		function(){
		$(this).css({
			backgroundColor: "yellow"
		})
	}, 
		function(){
		$(this).css({
			backgroundColor: "white"
		})
	});
});

$(document).ready(function() {
	$("h2")
		.click(function() {
			$(this).animate({
				opacity: "0.25",
				marginLeft: "20px"
			}, {
				duration: "slow",
				complete: function() {$("div.speech").fadeTo("slow", 0.5)}
			});
		});
});

$(document).ready(function() {
	$(document).keyup(function(event) {
		var $switcher = $('div#switcher');
		var keypress = event.which;
		if (keypress == 37) {
			$switcher
				.css({position:"relative"})
				.animate({
					right: "+=20px"
				}, {
				duration: "slow"
			});
		}
		else if (keypress == 38) {
			$switcher
				.css({position:"relative"})
				.animate({
					bottom: "+=20px"
				}, {
				duration: "slow"
			});
		}
		if (keypress == 39) {
			$switcher
				.css({position:"relative"})
				.animate({
					right: "-=20px"
				}, {
				duration: "slow"
			});
		}
		if (keypress == 40) {
			$switcher
				.css({position:"relative"})
				.animate({
					bottom: "-=20px"
				}, {
				duration: "slow"
			});
		}
	});
});