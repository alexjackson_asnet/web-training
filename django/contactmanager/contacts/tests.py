from django.test import TestCase
from contacts.models import Contact
from django.test.client import Client, RequestFactory
from contacts.views import ListContactView
# Create your tests here.

# Tests for the contact class model
class ContactTests(TestCase):
	def test_str(self):
		contact = Contact(first_name="Jimmy", last_name="Bean")
		self.assertEquals(
			str(contact),
			'Jimmy Bean',
		)

# Tests for the contact class views
class ContactViewTests(TestCase):
	def client_test_contacts(self):
		client = Client()
		response = client.get('/')

		self.assertEquals(list(response.context['object_list']), [])

		Contact.objects.create(firest_name='Jimmy', last_name='Bean')
		response = client.get('/')
		self.assertEquals(response.context['object_list'].count(), 1)

	def request_factory_test_contacts(self):
		factory = RequestFactory()
		request = factory.get('/')

		response = ListContactView.as_view() (request)

		self.assertEquals(list(response.context_data['object_list']), [])

		Contact.objects.create(first_name='Jimmy', last_name='Bean')
		response = ListContactView.as_view() (request)
		self.assertEquals(response.context_data['object_list'].count(), 1)