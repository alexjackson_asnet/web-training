def right_justify(s):
	modded = len(s) % 70
	spaces = ''
	if (modded < 70):
		spaces = (' ' * (70 - modded))
	print spaces + s

# right_justify('cat')

def do_twice(f, value):
	f(value) 
	f(value)

def printer(value):
	print value

# do_twice(printer, 'sat')

def print_grid(width, height):
	for x in range(0, height):
		print_width(width)
		print (('|        ' * width) + '|\n') * 4, 
		if ( x == height - 1):
			print_width(width)

def print_width(width):
	print ('+ - - - -' * width) + '+'

# print_grid(2, 2) 