def count_chars(word):
	dict_1 = dict()
	for c in word:
		dict_1[c] = dict_1.get(c, 0) + 1
	return dict_1

#print count_chars('banana man')

def print_dict(dict_1):
	keys_1 = dict_1.keys()
	keys_1.sort()
	for key in keys_1:
		print key, dict_1[key]

#print_dict(count_chars('banana man'))

known = {0:0, 1:1}
def memoize_fib(n):
	if n in known:
		return known[n]
	res = memoize_fib(n-1) + memoize_fib(n-2)
	known[n] = res
	return res

#print memoize_fib(50)
