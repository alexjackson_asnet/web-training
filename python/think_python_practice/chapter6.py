def is_pali(word):
	if (len(word) == 0 or len(word) == 1):
		return True
	elif (word[0] == word[-1]):
		return is_pali(word[1:-1])
	else:
		return False

#word = 'racecar'
#if (is_pali(word)):
#	print 'Yes'
#else:
#	print 'No'


def is_power(a, b):
	a = float(a) # needed to base case to perform as desired.
	b = float(b)
	if (a == 1 or a == b):
		return True
	if (a < b):
		return False
	else:
		return is_power((a/b), b)

def test_is_power(a, b):
	if (is_power(a, b)):
		print 'Yes'
	else:
		print 'No'

# test_is_power(130, 5)