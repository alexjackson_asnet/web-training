def is_anagram(word1, word2):
	word1, word2 = list(word1), list(word2)
	word1.sort()
	word2.sort()
	return word1 == word2

#if (is_anagram('rando', 'odanr')):
#	print 'yes'
#else: 
#	print 'no'

# Really hacky parameters, trying to find a better way to have the bool_func function
# work with different functions that take different numbers of arguements- solution so far
# is to fill up all functions with an optional parameter with a "None" value to start: not ideal
def dupes(li, filler_arg=None):
	compare = []
	for x in li:
		for y in compare:
			if x == y:
				return True
		compare.append(x)
	return False

list_1 = [8, 4, 4, 3, 20, 6]

def bool_func(function, arg1, arg2=None):
	if (function(arg1, arg2)):
		print 'Yes'
	else: 
		print 'No'

#bool_func(dupes, list_1)

list_1.sort()

def quick_search(list_1, searched):
	length = len(list_1)
	if (length == 0):
		return False
	middle = length/2 
	if (list_1[middle] == searched):
		return True
	if (list_1[middle] > searched):
		return quick_search(list_1[:middle], searched)
	else: 
		return quick_search(list_1[middle+1:], searched)

#bool_func(quick_search, list_1, 15)

