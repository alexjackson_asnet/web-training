from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic import ListView, CreateView
from contacts.models import Contact
from django.core.urlresolvers import reverse
# Create your views here.

class ListContactView(ListView):
	model = Contact
	template_name = 'contact_list.html'

class CreateContactView(CreateView):
	model = Contact
	fields = ['first_name', 'last_name', 'email']
	template_name = 'contacts/edit_contact.html'
	def get_success_url(self):
		return reverse('contacts-list')
