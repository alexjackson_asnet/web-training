Readme for Practice 1 of web training
Created:
	2015 05 25 
	By Alex Jackson

This project consists of 5 major documents in addition to the documents included in all projects
(.gitignore file, .csscomb.json file, etc.)

The main documents are as follows:

index.html:
Home/landing page for the Greensbury Market organic meat market website

main_header.css:
style page for the header for the website that is shared by all major links included in the main 
navigation bar

greensbury_home.css:
stylesheet for the content part of the home page

contact.html:
html information for the contact page

contact.css:
stylesheet for the content of the contact page