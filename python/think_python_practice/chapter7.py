import math
def sqrt(a):
	eps = 0.0000001
	x = float(a/3)
	while True:
		y = (x + float(a/x)) / 2
		if (abs(y-x) < eps):
			break
		x = y
	return x

for i in range(1, 10):
	x = sqrt(float(i))
	y = math.sqrt(float(i))
	print float(i), x, y, abs(x-y) 